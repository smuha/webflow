<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>

    <style>
        .registrationForm{
            width: 200px;
            margin: auto;
            margin: auto;
        }​
    </style>
</head>

<body>
<h2>Generate secure code</h2><hr>
<div class="registrationForm">
    <g:form>
        <table style="text-align: center">
            <tr>
                <td>
                    <input type="password" id="secureCode" placeholder="Secure code">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="password" id="confirmedSecureCode" placeholder="Confirm secure code">
                </td>
            </tr>
        </table>
        <g:submitButton name="back" value="Back" />
        <g:submitButton name="next" value="Next step" />
    </g:form>
</div>
</body>
</html>