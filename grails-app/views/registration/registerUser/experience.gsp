<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>
    <style>
        .registrationForm{
            width: 200px;
            margin: auto;
            margin: auto;
        }​
    </style>
<body>
<h2>Experience</h2><hr>
<div class="registrationForm">
    <g:form>
        <table>
            <tr>
                <td><input type="checkbox" id="java"><label for="java">Java</label></td>
            </tr>
            <tr>
                <td><input type="checkbox" id="groovy"><label for="groovy">Groovy</label></td>
            </tr>
            <tr>
                <td><input type="checkbox" id="ruby"><label for="ruby">Ruby</label></td>
            </tr>
            <tr>
                <td><input type="checkbox" id="spring"><label for="spring">Spring</label></td>
            </tr>
            <tr>
                <td><input type="checkbox" id="hibernate"><label for="hibernate">Hibernate</label></td>
            </tr>
        </table>
        <g:submitButton name="back" value="Back" />
        <g:submitButton name="complete" value="Complete registration" />
    </g:form>
</div>
</body>
</html>