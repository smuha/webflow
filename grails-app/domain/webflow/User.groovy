package webflow

class User {

    static constraints = {
    }

    String firstName
    String lastName
    String sex
    Date birthday
    String securityCode
}
