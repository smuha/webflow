package webflow

class RegistrationController {

    def index() { }

    def registerUserFlow = {
        personalData {
            on("cancel").to "index"
            on("next").to "generateSecureCode"
        }

        generateSecureCode {
            on("back").to "personalData"
            on("next").to "experience"
        }

        experience {
            on("back").to "generateSecureCode"
            on("complete").to "complete"
        }

        complete {
            action{
                
            }
            render(view: "complete")
        }
    }
}
